from check_normalization_effect import get_syst_to_largest_norm_effect_dict
from process_p_values import get_p_values_dict, get_dict_syst_to_smallest_pvalue_and_file_name

from make_plots import region_dict, sample_dict
from plotting_modules.comparison_plot import ComparisonPlot

from sys import argv

from ROOT import TFile

def print_syst_as_latex_table(lines_of_table : list[str,float,float], frame_title : str) -> None:
    end_of_slide = """
        \hline

        \end{tabular}
        \end{tiny}
    \end{table}


\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

    counter = 0
    for line in lines_of_table:
        if counter % 30 == 0:
            print("""

\\begin{frame}
    \\frametitle{"""+frame_title+"""}


    \\vspace{-0.5cm}
    \\begin{table}[!h]
        \\begin{tiny}
        \\begin{tabular}{|l|c|c|}
        \\hline

        Systematic uncertainty & KS test p-value & Norm. effect \\\\ \\hline
""")
        systematic_name = line[0]
        if systematic_name == "NOSYS":
            continue
        p_value = round(line[1], 4)
        normalization_effect = round(line[2],4)
        latex_name = systematic_name.replace("_", "$\_$").replace("$$", "")
        print(f"\t\t{latex_name} & {100*p_value}$\%$ & {normalization_effect}$\%$ \\\\")

        if counter % 30 == 29:
            print(end_of_slide)
        counter += 1

    if counter % 30 != 29:
        print(end_of_slide)

def get_plots_latex_templates(plots : list[str]) -> None:
    begin_slide = """
\\begin{frame}
    \\frametitle{Plots for the worst variable, region and sample}
    \\begin{center}
"""

    end_slide = """
        \\end{center}
    \\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

    counter = 0
    for plot in plots:
        if counter % 2 == 0:
            print(begin_slide)
        print(f"\t\t\t\\includegraphics[width=0.49\\textwidth]{{/home/dubovsky/Analysis/4L/overlap_removal_studies/output/{plot}}}")
        if counter % 2 == 1:
            print(end_slide)
        counter += 1
    if counter % 2 == 1:
        print(end_slide)

def create_plot(systematic : str, histogram_name : str, root_file_name : str, root_files_dir : str, output_dir) -> None:
    file_all_syst     = TFile(root_files_dir + "/AllSysts/" + root_file_name)
    file_nominal_only = TFile(root_files_dir + "/NominalOnly/" + root_file_name)
    histo_all_syst      = file_all_syst.Get(systematic + "/" + histogram_name)
    histo_nominal_only  = file_nominal_only.Get(systematic + "/" + histogram_name)
    nominal_all_syst = file_all_syst.Get("NOSYS/" + histogram_name)
    nominal_nom_only = file_nominal_only.Get("NOSYS/" + histogram_name)

    # apply nominal-only systematic on all syst nominal
    histo_nominal_only_adjusted = (histo_nominal_only - nominal_nom_only) + nominal_all_syst
    histo_nominal_only_adjusted.SetDirectory(0)

    region = histogram_name.split("_")[-1]
    region_label = region_dict[region]
    variable_name = histogram_name[:-len(region)-1]
    comparison_plot = ComparisonPlot(histo_nominal_only_adjusted, histo_all_syst, nominal_all_syst, variable_name, region_label, root_file_name.replace(".root",""), systematic)
    comparison_plot.save(output_dir + "/" + sample_dict[root_file_name] + "_" + systematic + "_" + histogram_name + ".png")

def get_systematics_to_smallest_pvalue_key(p_value_dict : dict[tuple[int, str,str,str], float]) -> dict[str, tuple[int,str,str,str]]:
    """
    @file_name: text file with p-values
    @return: dictionary with p-values as values and keys as tuples of (dsid, systematics, region, variable)
    """
    result = {}
    dict_syst_to_smalles_pvalue = {}
    for key, value in p_value_dict.items():
        syst = key[1]
        if syst not in result:
            result[syst] = key
            dict_syst_to_smalles_pvalue[syst] = value
        else:
            if value < dict_syst_to_smalles_pvalue[syst]:
                result[syst] = key
                dict_syst_to_smalles_pvalue[syst] = value
    return result


if __name__ == "__main__":
    normalization_effect_threshold = 0.0005
    p_value_threshold = 0.98

    p_value_text_file = argv[1]
    p_value_dict = get_p_values_dict(p_value_text_file)
    syst_to_smallest_pvalue = get_dict_syst_to_smallest_pvalue_and_file_name(p_value_dict)
    syst_to_smallest_pvalue_key = get_systematics_to_smallest_pvalue_key(p_value_dict)

    root_files_dir = argv[2]
    normalization_effects = get_syst_to_largest_norm_effect_dict(root_files_dir)

    sorted_systematics_names = list(normalization_effects.keys())
    sorted_systematics_names.sort()

    affected_systematics_tuples = [
        (systematic_name, syst_to_smallest_pvalue[systematic_name][0], 100*normalization_effects[systematic_name])
        for systematic_name in sorted_systematics_names
        if (systematic_name != "NOSYS" and (syst_to_smallest_pvalue[systematic_name][0] < p_value_threshold or normalization_effects[systematic_name] > normalization_effect_threshold))
    ]
    affected_systematics_names = [x[0] for x in affected_systematics_tuples]


    plots = [
        syst_to_smallest_pvalue[systematic_name][1]
        for systematic_name in sorted_systematics_names
        if (systematic_name != "NOSYS" and (syst_to_smallest_pvalue[systematic_name][0] < p_value_threshold or normalization_effects[systematic_name] > normalization_effect_threshold))
    ]

    print_syst_as_latex_table(affected_systematics_tuples, "Affected uncertainties:")


    not_affected_systematics = [
        (systematic_name, syst_to_smallest_pvalue[systematic_name][0], 100*normalization_effects[systematic_name])
        for systematic_name in sorted_systematics_names
        if (systematic_name != "NOSYS" and not (syst_to_smallest_pvalue[systematic_name][0] < p_value_threshold or normalization_effects[systematic_name] > normalization_effect_threshold))
    ]

    print_syst_as_latex_table(not_affected_systematics, "Not-affected uncertainties:")

    counter = 0


    dsid_to_filename_dict = {}
    for root_file in sample_dict:
        dsid = sample_dict[root_file]
        dsid_to_filename_dict[dsid] = root_file

    # create plots:
    for affected_systematic in affected_systematics_tuples:
        systematics_name = affected_systematic[0]
        key = syst_to_smallest_pvalue_key[systematics_name]
        dsid = key[0]
        systematics = key[1]
        region = key[2]
        variable = key[3]
        create_plot(systematics, variable+"_"+region, dsid_to_filename_dict[dsid], root_files_dir, argv[3])



    print("\n\n\n")
    print("Plots:")
    get_plots_latex_templates(plots)



