from sys import argv
import os
from plotting_modules.comparison_plot import ComparisonPlot
from plotting_modules.set_atlas_style import SetAtlasStyle

from ROOT import TFile

region_dict = {
    "Electron": "e+jets",
    "Muon": "#mu+jets",
}

sample_dict = {
    "ttbar_PowhegHerwig7.root" : "411233",
    "ttbar_PowhegPythiaNominal.root" : "410470",
    "ttbar_sherpa.root" : "700662",
}

if __name__ == "__main__":
    #SetAtlasStyle()
    histo_files_dir = argv[1]
    output_dir = argv[2]

    samples = os.listdir(histo_files_dir + "/AllSysts")
    pvalue_dict = {}

    for sample in samples:
        nominal_histograms = {}
        #if sample != "ttbar_PowhegHerwig7.root":
        #    continue
        file_all_syst     = TFile(histo_files_dir + "/AllSysts/" + sample)
        file_nominal_only = TFile(histo_files_dir + "/NominalOnly/" + sample)
        systematic_list = file_all_syst.GetListOfKeys()
        histogram_names = file_all_syst.Get("NOSYS").GetListOfKeys()
        for histo_name in histogram_names:
            nominal_histograms[histo_name.GetName()] = file_nominal_only.Get("NOSYS/" + histo_name.GetName())

        for systematic in systematic_list:
            if systematic.GetName() == "NOSYS":
                continue
            for histogram_name in histogram_names:
                histo_all_syst      = file_all_syst.Get(systematic.GetName() + "/" + histogram_name.GetName())
                histo_nominal_only  = file_nominal_only.Get(systematic.GetName() + "/" + histogram_name.GetName())
                nominal = nominal_histograms[histogram_name.GetName()]
                region = histogram_name.GetName().split("_")[-1]
                region_label = region_dict[region]
                variable_name = histogram_name.GetName()[:-len(region)-1]
                comparison_plot = ComparisonPlot(histo_nominal_only, histo_all_syst, nominal, variable_name, region_label, sample.replace(".root",""), systematic.GetName())
                comparison_plot.save(output_dir + "/" + sample_dict[sample] + "_" + systematic.GetName() + "_" + histogram_name.GetName() + ".png")
                ks_value = histo_nominal_only.KolmogorovTest(histo_all_syst)
                key_tuple = (sample_dict[sample], systematic.GetName(), region, variable_name)
                pvalue_dict[key_tuple] = ks_value

    #exit()

    with open(output_dir + "/pvalues.txt", "w") as f:
        for key, value in pvalue_dict.items():
            f.write(str(key) + " " + str(round(value,5)) + "\n")


    with open(output_dir + "/pvalues_sorted.txt", "w") as f:
        value_key_list = [(value, key) for key, value in pvalue_dict.items()]
        value_key_list.sort(key=lambda x:x[0])

        for value_key in value_key_list:
            f.write(str(value_key[1]) + " " + str(round(value_key[0],5)) + "\n")