from check_normalization_effect import get_syst_to_largest_norm_effect_dict
from process_p_values import get_p_values,get_dict_syst_to_smallest_pvalue_and_file_name

from sys import argv

def print_syst_as_latex_table(lines_of_table : list[str,float,float], frame_title : str) -> None:
    end_of_slide = """
        \hline

        \end{tabular}
        \end{tiny}
    \end{table}


\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

    counter = 0
    for line in lines_of_table:
        if counter % 30 == 0:
            print("""

\\begin{frame}
    \\frametitle{"""+frame_title+"""}


    \\vspace{-0.5cm}
    \\begin{table}[!h]
        \\begin{tiny}
        \\begin{tabular}{|l|c|c|}
        \\hline

        Systematic uncertainty & KS test p-value & Norm. effect \\\\ \\hline
""")
        systematic_name = line[0]
        if systematic_name == "NOSYS":
            continue
        p_value = round(line[1], 4)
        normalization_effect = round(line[2],4)
        latex_name = systematic_name.replace("_", "$\_$").replace("$$", "")
        print(f"\t\t{latex_name} & {100*p_value}$\%$ & {normalization_effect}$\%$ \\\\")

        if counter % 30 == 29:
            print(end_of_slide)
        counter += 1

    if counter % 30 != 29:
        print(end_of_slide)

def get_plots_latex_templates(plots : list[str]) -> None:
    begin_slide = """
\\begin{frame}
    \\frametitle{Plots for the worst variable, region and sample}
    \\begin{center}
"""

    end_slide = """
        \\end{center}
    \\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""

    counter = 0
    for plot in plots:
        if counter % 2 == 0:
            print(begin_slide)
        print(f"\t\t\t\\includegraphics[width=0.49\\textwidth]{{/home/dubovsky/Analysis/4L/overlap_removal_studies/output/{plot}}}")
        if counter % 2 == 1:
            print(end_slide)
        counter += 1
    if counter % 2 == 1:
        print(end_slide)



if __name__ == "__main__":
    normalization_effect_threshold = 0.0005
    p_value_threshold = 0.98

    p_value_text_file = argv[1]
    p_value_dict = get_p_values(p_value_text_file)
    syst_to_smallest_pvalue = get_dict_syst_to_smallest_pvalue_and_file_name(p_value_dict)

    root_files_dir = argv[2]
    normalization_effects = get_syst_to_largest_norm_effect_dict(root_files_dir)

    sorted_systematics_names = list(normalization_effects.keys())
    sorted_systematics_names.sort()

    affected_systematics = [
        (systematic_name, syst_to_smallest_pvalue[systematic_name][0], 100*normalization_effects[systematic_name])
        for systematic_name in sorted_systematics_names
        if (systematic_name != "NOSYS" and (syst_to_smallest_pvalue[systematic_name][0] < p_value_threshold or normalization_effects[systematic_name] > normalization_effect_threshold))
    ]

    plots = [
        syst_to_smallest_pvalue[systematic_name][1]
        for systematic_name in sorted_systematics_names
        if (systematic_name != "NOSYS" and (syst_to_smallest_pvalue[systematic_name][0] < p_value_threshold or normalization_effects[systematic_name] > normalization_effect_threshold))
    ]

    print_syst_as_latex_table(affected_systematics, "Affected uncertainties:")


    not_affected_systematics = [
        (systematic_name, syst_to_smallest_pvalue[systematic_name][0], 100*normalization_effects[systematic_name])
        for systematic_name in sorted_systematics_names
        if (systematic_name != "NOSYS" and not (syst_to_smallest_pvalue[systematic_name][0] < p_value_threshold or normalization_effects[systematic_name] > normalization_effect_threshold))
    ]

    print_syst_as_latex_table(not_affected_systematics, "Not-affected uncertainties:")

    counter = 0

    print("\n\n\n")
    print("Plots:")
    get_plots_latex_templates(plots)

