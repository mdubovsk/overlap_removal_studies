from ROOT import TStyle, gROOT

atlas_style = None

def AtlasStyle():
    atlas_style = TStyle("","")

    # use plain black on white colors
    icol=0 # WHITE
    atlas_style.SetFrameBorderMode(icol)
    atlas_style.SetFrameFillColor(icol)
    atlas_style.SetCanvasBorderMode(icol)
    atlas_style.SetCanvasColor(icol)
    atlas_style.SetPadBorderMode(icol)
    atlas_style.SetPadColor(icol)
    atlas_style.SetStatColor(icol)
    #atlas_style.SetFillColor(icol) # don't use: white fill color for *all* objects

    # set the paper & margin sizes
    atlas_style.SetPaperSize(20,26)

    # set margin sizes
    atlas_style.SetPadTopMargin(0.05)
    atlas_style.SetPadRightMargin(0.05)
    atlas_style.SetPadBottomMargin(0.16)
    atlas_style.SetPadLeftMargin(0.16)

    # set title offsets (for axis label)
    atlas_style.SetTitleXOffset(1.4)
    atlas_style.SetTitleYOffset(1.4)

    # use large fonts
    #Int_t font=72 # Helvetica italics
    font=42 # Helvetica
    tsize=0.05
    #Double_t tsize=0.04 #Changed by Mato!!!
    atlas_style.SetTextFont(font)

    atlas_style.SetTextSize(tsize)
    atlas_style.SetLabelFont(font,"x")
    atlas_style.SetTitleFont(font,"x")
    atlas_style.SetLabelFont(font,"y")
    atlas_style.SetTitleFont(font,"y")
    atlas_style.SetLabelFont(font,"z")
    atlas_style.SetTitleFont(font,"z")

    atlas_style.SetLabelSize(tsize,"x")
    atlas_style.SetTitleSize(tsize,"x")
    atlas_style.SetLabelSize(tsize,"y")
    atlas_style.SetTitleSize(tsize,"y")
    atlas_style.SetLabelSize(tsize,"z")
    atlas_style.SetTitleSize(tsize,"z")

    # use bold lines and markers
    atlas_style.SetMarkerStyle(20)
    atlas_style.SetMarkerSize(1.2)    # default 1,2
    atlas_style.SetHistLineWidth(2)
    atlas_style.SetLineStyleString(2,"[12 12]") # postscript dashes

    # get rid of X error bars
    #atlas_style.SetErrorX(0)
    # get rid of error bar caps
    atlas_style.SetEndErrorSize(0.)

    # do not display any of the standard histogram decorations
    atlas_style.SetOptTitle(0)
    #atlas_style.SetOptStat(1111)
    atlas_style.SetOptStat(0)
    atlas_style.SetOptFit(1111)
    #atlas_style.SetOptFit(0)

    # put tick marks on top and RHS of plots
    atlas_style.SetPadTickX(1)
    atlas_style.SetPadTickY(1)

    return atlas_style



def SetAtlasStyle():
    global atlas_style
    if ( atlas_style!=None ):
        return
    print("\nApplying ATLAS style settings...\n")
    atlas_style = AtlasStyle()
    atlas_style.cd()
    #gROOT.SetStyle("ATLAS")
    gROOT.ForceStyle()
