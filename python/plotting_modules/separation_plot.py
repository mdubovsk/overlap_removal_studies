from ROOT import TH1D, TCanvas, TLatex, TPad, kRed, kBlue, gROOT, TLine, TMath
from enum import Enum
from copy import deepcopy

from plotting_modules.comparison_plot import get_maximum_value_plus_error_value, get_minimum_value_plus_error_value, get_chi2_pvalue, get_maximum_value_ratio, get_minimum_value_ratio

class SeparationPlot:
    _atlas_style_set = False

    def __init__(self, plot1 : TH1D, plot2 : TH1D, variable_name : str, region_name : str, sample : str, systematics : str):
        if SeparationPlot._atlas_style_set == False:
            #SetAtlasStyle()
            SeparationPlot._atlas_style_set = True
            gROOT.SetBatch(1)

        self.variable_name = variable_name
        self.region_name = region_name
        self.sample = sample
        self.systematics = systematics

        self.plot1 = deepcopy(plot1)
        self.plot2 = deepcopy(plot2)

        self.legend1 = "NominalOnly OR, noiminal var."
        self.legend2 = "AllSysts OR, noiminal var."

        # get kolmogorov-smirnov test result
        self.ks_test_result = self.plot1.KolmogorovTest(self.plot2, "X")
        self.chi2_pvalue = get_chi2_pvalue(self.plot1, self.plot2)

        y_min = min([get_minimum_value_plus_error_value(histo) for histo in [self.plot1, self.plot2]])
        y_max = max([get_maximum_value_plus_error_value(histo) for histo in [self.plot1, self.plot2]])
        y_range = y_max - y_min
        y_min = max(0,y_min-0.2*y_range)
        y_max += 0.8*y_range

        self.plot1.GetYaxis().SetRangeUser(y_min, y_max)
        self.plot1.SetTitle("")
        self.plot1.SetStats(0)

        self.plot2.GetYaxis().SetRangeUser(y_min, y_max)
        self.plot2.SetTitle("")
        self.plot2.SetStats(0)

    def set_legend(self, legend1 : str, legend2 : str):
        self.legend1 = legend1
        self.legend2 = legend2

    def set_y_range_no_overlap(self, y_min : float, y_max : float):
        y_range = y_max - y_min
        y_min = max(0,y_min-0.2*y_range)
        y_max += 0.4*y_range
        self.plot1.GetYaxis().SetRangeUser(y_min, y_max)
        self.plot2.GetYaxis().SetRangeUser(y_min, y_max)

    def set_y_range(self, y_min : float, y_max : float):
        self.plot1.GetYaxis().SetRangeUser(y_min, y_max)
        self.plot2.GetYaxis().SetRangeUser(y_min, y_max)

    def save(self, output_address : str, width : int = 600, height : int = 900):
        c1 = TCanvas("","",width,height)
        c1.cd()

        pad_main_plot = TPad("","",0.0,0.3,1,1)
        pad_main_plot.SetLeftMargin(0.2)
        pad_main_plot.SetRightMargin(0.04)
        pad_main_plot.SetTopMargin(0.06)
        pad_main_plot.SetBottomMargin(0)
        pad_main_plot.Draw()
        pad_main_plot.cd()

        self.plot1.SetLineColor(2)
        self.plot1.SetLineWidth(1)

        self.plot2.SetLineColor(4)
        self.plot2.SetLineWidth(1)

        self.plot1.GetYaxis().SetTitle("Events")
        self.plot1.GetXaxis().SetTitleOffset(50)
        self.plot1.SetLabelSize( 0.05, "Y" )
        self.plot1.SetTitleSize( 0.05, "Y" )

        self.plot1.Draw("h")
        self.plot2.Draw("h,same")
        self._add_atlas_internal()
        self._add_cme_lumi_label()
        self._add_region_label()
        self._add_sample()
        self._add_systematics()
        self._add_legend()
        self._add_ks_test_result()

        c1.cd()
        ratio_pad = TPad("","",0.0,0.02,1,0.3)
        ratio_pad.SetLeftMargin(0.2)
        ratio_pad.SetRightMargin(0.04)
        ratio_pad.SetTopMargin(0)
        ratio_pad.SetBottomMargin(0.3)
        ratio_pad.Draw()
        ratio_pad.cd()

        ratio_histogram = self.plot2/self.plot1
        ratio_histogram.GetYaxis().SetRangeUser(0.98,1.02)
        if self.ks_test_result < 0.98:
            y_min = get_minimum_value_ratio(ratio_histogram)
            y_max = get_maximum_value_ratio(ratio_histogram)
            y_range = y_max - y_min
            if y_range == 0:
                y_range = 0.04
            y_min -= 0.2*y_range
            y_max += 0.2*y_range
            ratio_histogram.GetYaxis().SetRangeUser(y_min, y_max)

        ratio_histogram.GetYaxis().SetTitle("#frac{" + self.legend2 + "}{" + self.legend1 + "}")
        ratio_histogram.GetXaxis().SetTitle(self.plot1.GetXaxis().GetTitle())
        ratio_histogram.SetLineColor(1)
        ratio_histogram.SetLabelSize( 0.12, "X" )
        ratio_histogram.SetLabelSize( 0.10, "Y" )
        ratio_histogram.SetTitleSize( 0.12, "X" )
        ratio_histogram.SetTitleSize( 0.07, "Y" )
        ratio_histogram.SetTitleOffset( 1.1, "X" )
        ratio_histogram.SetTitleOffset( 1.35, "Y" )
        ratio_histogram.SetMarkerStyle(8)
        ratio_histogram.SetMarkerSize(0.7)

        ratio_histogram.Draw()

        line_reference_one = TLine(ratio_histogram.GetXaxis().GetXmin(), 1, ratio_histogram.GetXaxis().GetXmax(), 1)
        line_reference_one.SetLineColor(1)
        line_reference_one.SetLineStyle(2)
        line_reference_one.Draw()

        c1.Print(output_address)


    def _add_atlas_internal(self):
        atlas_label = TLatex()
        atlas_label.SetTextAlign()
        atlas_label.SetTextSize(0.048)
        atlas_label.SetNDC()
        atlas_label.SetTextFont(72)
        atlas_label.DrawLatex(0.23, 0.89, "ATLAS")
        atlas_label.SetTextFont(42)
        atlas_label.SetTextSize(0.043)
        atlas_label.DrawLatex(0.40, 0.89, "Internal")

    def _add_cme_lumi_label(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.23, 0.84, "#sqrt{s} = 13 TeV")

    def _add_region_label(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.60, 0.89, self.region_name)

    def _add_sample(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.60, 0.84, self.sample)

    def _add_legend(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.23, 0.79, "#color[2]{" + self.legend1 + "}")
        cme_lumi_label.DrawLatex(0.23, 0.74, "#color[4]{" + self.legend2 + "}")

    def _add_systematics(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.035)
        cme_lumi_label.DrawLatex(0.23, 0.64, self.systematics)

    def _add_ks_test_result(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        #cme_lumi_label.DrawLatex(0.23, 0.74, "Kolmogorov-Smirnov p-value: " + str(round(self.ks_test_result,4)))
        cme_lumi_label.DrawLatex(0.23, 0.69, "p(#chi^{2}/ndf) = " + str(round(self.chi2_pvalue,4)))
