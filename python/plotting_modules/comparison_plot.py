from ROOT import TH1D, TCanvas, TLatex, TPad, kRed, kBlue, gROOT, TLine, TMath
from enum import Enum
from copy import deepcopy

def get_maximum_value_plus_error_value(histo : TH1D) -> float:
    result = histo.GetBinContent(1) + histo.GetBinError(1)
    for i in range(2, histo.GetNbinsX()+1):
        value = histo.GetBinContent(i) + histo.GetBinError(i)
        if value > result:
            result = value
    return result

def get_minimum_value_plus_error_value(histo : TH1D) -> float:
    result = histo.GetBinContent(1) - histo.GetBinError(1)
    for i in range(2, histo.GetNbinsX()+1):
        value = histo.GetBinContent(i) - histo.GetBinError(i)
        if value < result:
            result = value
    return result

def get_maximum_value_ratio(histo : TH1D) -> float:
    result = histo.GetBinContent(1)
    for i in range(2, histo.GetNbinsX()+1):
        value = histo.GetBinContent(i)
        if value > result:
            result = value
    return result

def get_minimum_value_ratio(histo : TH1D) -> float:
    result = None
    for i in range(1, histo.GetNbinsX()+1):
        if histo.GetBinContent(i) == 0:
            continue
        if result == None:
            result = histo.GetBinContent(i)
        value = histo.GetBinContent(i)
        if value < result:
            result = value
    if result:
        return result
    return 0

def get_chi2_pvalue(histo1 : TH1D, histo2 : TH1D) -> float:
    chi2 = 0
    ndf = 0
    for i in range(1, histo1.GetNbinsX()+1):
        if histo1.GetBinContent(i) < 0 or histo2.GetBinContent(i) < 0:
            continue
        if histo1.GetBinContent(i) == 0 and histo2.GetBinContent(i) == 0:
            continue
        if (histo1.GetBinError(i)**2 + histo2.GetBinError(i)**2 < 1e-6):
            continue
        chi2 += (histo1.GetBinContent(i) - histo2.GetBinContent(i))**2 / (histo1.GetBinError(i)**2 + histo2.GetBinError(i)**2)
        ndf += 1
    return TMath.Prob(chi2, ndf)

class ComparisonPlot:
    _atlas_style_set = False

    def __init__(self, histogram_nominal_or : TH1D, histogram_all_systs : TH1D, nominal_histo : TH1D, variable_name : str, region_name : str, sample : str, systematics : str):
        if ComparisonPlot._atlas_style_set == False:
            #SetAtlasStyle()
            ComparisonPlot._atlas_style_set = True
            gROOT.SetBatch(1)

        self.variable_name = variable_name
        self.region_name = region_name
        self.sample = sample
        self.systematics = systematics

        self.histogram_nominal_or   = deepcopy(histogram_nominal_or)
        self.histogram_all_systs = deepcopy(histogram_all_systs)
        self.histo_nominal = deepcopy(nominal_histo)

        # get kolmogorov-smirnov test result
        self.ks_test_result = 0
        #self.ks_test_result = self.histogram_nominal_or.KolmogorovTest(self.histogram_all_systs, "X")
        self.chi2_pvalue = get_chi2_pvalue(self.histogram_nominal_or, self.histogram_all_systs)

        y_min = min([get_minimum_value_plus_error_value(histo) for histo in [self.histogram_nominal_or, self.histogram_all_systs, self.histo_nominal]])
        y_max = max([get_maximum_value_plus_error_value(histo) for histo in [self.histogram_nominal_or, self.histogram_all_systs, self.histo_nominal]])
        y_range = y_max - y_min
        y_min = max(0,y_min-0.2*y_range)
        y_max += 0.6*y_range

        self.histogram_nominal_or.GetYaxis().SetRangeUser(y_min, y_max)
        self.histogram_nominal_or.SetTitle("")
        self.histogram_nominal_or.SetStats(0)

        self.histogram_all_systs.GetYaxis().SetRangeUser(y_min, y_max)
        self.histogram_all_systs.SetTitle("")
        self.histogram_all_systs.SetStats(0)

        self.histo_nominal.GetYaxis().SetRangeUser(y_min, y_max)
        self.histo_nominal.SetTitle("")
        self.histo_nominal.SetStats(0)


    def set_y_range_no_overlap(self, y_min : float, y_max : float):
        y_range = y_max - y_min
        y_min = max(0,y_min-0.2*y_range)
        y_max += 0.4*y_range
        self.histogram_nominal_or.GetYaxis().SetRangeUser(y_min, y_max)
        self.histogram_all_systs.GetYaxis().SetRangeUser(y_min, y_max)

    def set_y_range(self, y_min : float, y_max : float):
        self.histogram_nominal_or.GetYaxis().SetRangeUser(y_min, y_max)
        self.histogram_all_systs.GetYaxis().SetRangeUser(y_min, y_max)

    def save(self, output_address : str, width : int = 600, height : int = 900):
        c1 = TCanvas("","",width,height)
        c1.cd()

        pad_main_plot = TPad("","",0.0,0.45,1,1)
        pad_main_plot.SetLeftMargin(0.15)
        pad_main_plot.SetRightMargin(0.04)
        pad_main_plot.SetTopMargin(0.06)
        pad_main_plot.SetBottomMargin(0)
        pad_main_plot.Draw()
        pad_main_plot.cd()

        self.histogram_nominal_or.SetLineColor(2)
        self.histogram_nominal_or.SetLineWidth(1)

        self.histogram_all_systs.SetLineColor(4)
        self.histogram_all_systs.SetLineWidth(1)

        self.histo_nominal.SetLineColor(3)
        self.histo_nominal.SetLineWidth(1)

        self.histogram_nominal_or.GetYaxis().SetTitle("Events")
        self.histogram_nominal_or.GetXaxis().SetTitleOffset(50)
        self.histogram_nominal_or.SetLabelSize( 0.05, "Y" )
        self.histogram_nominal_or.SetTitleSize( 0.05, "Y" )

        self.histogram_nominal_or.Draw("h")
        self.histogram_all_systs.Draw("h,same")
        self.histo_nominal.Draw("h,same")
        self._add_atlas_internal()
        self._add_cme_lumi_label()
        self._add_region_label()
        self._add_sample()
        self._add_systematics()
        self._add_legend()
        self._add_ks_test_result()

        c1.cd()
        ratio_plot_allsyst_over_nominal_or = TPad("","",0.0,0.22,1,0.45)
        ratio_plot_allsyst_over_nominal_or.SetLeftMargin(0.15)
        ratio_plot_allsyst_over_nominal_or.SetRightMargin(0.04)
        ratio_plot_allsyst_over_nominal_or.SetTopMargin(0)
        ratio_plot_allsyst_over_nominal_or.SetBottomMargin(0.0)
        ratio_plot_allsyst_over_nominal_or.Draw()
        ratio_plot_allsyst_over_nominal_or.cd()

        ratio_all_syst_to_nominam_only = self.histogram_all_systs/self.histogram_nominal_or
        ratio_all_syst_to_nominam_only.GetYaxis().SetRangeUser(0.98,1.02)
        if self.ks_test_result < 0.98:
            y_min = get_minimum_value_ratio(ratio_all_syst_to_nominam_only)
            y_max = get_maximum_value_ratio(ratio_all_syst_to_nominam_only)
            y_range = y_max - y_min
            if y_range == 0:
                y_range = 0.04
            y_min -= 0.2*y_range
            y_max += 0.2*y_range
            ratio_all_syst_to_nominam_only.GetYaxis().SetRangeUser(y_min, y_max)

        ratio_all_syst_to_nominam_only.GetYaxis().SetTitle("#frac{AllSysts}{NominalOnly OR}, #color[4]{#frac{AllSysts}{NominalOnly}}")
        ratio_all_syst_to_nominam_only.GetXaxis().SetTitle(self.histogram_nominal_or.GetXaxis().GetTitle())
        ratio_all_syst_to_nominam_only.SetLineColor(1)
        ratio_all_syst_to_nominam_only.SetLabelSize( 0.12, "X" )
        ratio_all_syst_to_nominam_only.SetLabelSize( 0.10, "Y" )
        ratio_all_syst_to_nominam_only.SetTitleSize( 0.12, "X" )
        ratio_all_syst_to_nominam_only.SetTitleSize( 0.07, "Y" )
        ratio_all_syst_to_nominam_only.SetTitleOffset( 1.1, "X" )
        ratio_all_syst_to_nominam_only.SetTitleOffset( 0.95, "Y" )
        ratio_all_syst_to_nominam_only.SetMarkerStyle(8)
        ratio_all_syst_to_nominam_only.SetMarkerSize(0.7)

        ratio_all_syst_to_nominam_only.Draw()

        ratio_all_syst_to_nominal = self.histogram_all_systs/self.histo_nominal
        ratio_all_syst_to_nominal.SetLineColor(4)
        for i_bin in range(1, ratio_all_syst_to_nominal.GetNbinsX()+1):
            ratio_all_syst_to_nominal.SetBinError(i_bin, 0)

        line_reference_one = TLine(ratio_all_syst_to_nominam_only.GetXaxis().GetXmin(), 1, ratio_all_syst_to_nominam_only.GetXaxis().GetXmax(), 1)
        line_reference_one.SetLineColor(1)
        line_reference_one.SetLineStyle(2)
        line_reference_one.Draw()
        ratio_all_syst_to_nominal.Draw("same,h")

        c1.cd()
        pad_ratio_effect_vs_systematics = TPad("","",0,0.02,1,0.22)
        pad_ratio_effect_vs_systematics.SetLeftMargin(0.15)
        pad_ratio_effect_vs_systematics.SetRightMargin(0.04)
        pad_ratio_effect_vs_systematics.SetTopMargin(0)
        pad_ratio_effect_vs_systematics.SetBottomMargin(0.3)
        pad_ratio_effect_vs_systematics.Draw()
        pad_ratio_effect_vs_systematics.cd()

        ratio_relative_effect = (self.histogram_all_systs-self.histogram_nominal_or)/(self.histogram_all_systs - self.histo_nominal)
        ratio_relative_effect.GetYaxis().SetRangeUser(0.98,1.02)

        y_min = get_minimum_value_ratio(ratio_relative_effect)
        y_max = get_maximum_value_ratio(ratio_relative_effect)
        y_range = y_max - y_min
        if y_range == 0:
            y_range = 0.04
        y_min -= 0.2*y_range
        y_max += 0.2*y_range
        y_min = min(-0.5, y_min)
        y_min = max(-1.0, y_min)
        y_max = max(1.5, y_max)
        y_max = min(2.0, y_max)

        ratio_relative_effect.GetYaxis().SetRangeUser(y_min, y_max)

        ratio_relative_effect.GetYaxis().SetTitle("#frac{#color[4]{AllSysts} - #color[2]{NominalOnly OR}}{#color[4]{AllSysts} - #color[3]{Nominal}}")
        ratio_relative_effect.GetXaxis().SetTitle(self.histogram_nominal_or.GetXaxis().GetTitle())
        ratio_relative_effect.SetLineColor(1)
        ratio_relative_effect.SetLabelSize( 0.12, "X" )
        ratio_relative_effect.SetLabelSize( 0.10, "Y" )
        ratio_relative_effect.SetTitleSize( 0.12, "X" )
        ratio_relative_effect.SetTitleSize( 0.07, "Y" )
        ratio_relative_effect.SetTitleOffset( 1.1, "X" )
        ratio_relative_effect.SetTitleOffset( 0.95, "Y" )

        ratio_relative_effect.Draw()

        line_reference_zero = TLine(ratio_relative_effect.GetXaxis().GetXmin(), 0, ratio_relative_effect.GetXaxis().GetXmax(), 0)
        line_reference_zero.SetLineColor(1)
        line_reference_zero.SetLineStyle(2)
        line_reference_zero.Draw()


        c1.Print(output_address)


    def _add_atlas_internal(self):
        atlas_label = TLatex()
        atlas_label.SetTextAlign()
        atlas_label.SetTextSize(0.048)
        atlas_label.SetNDC()
        atlas_label.SetTextFont(72)
        atlas_label.DrawLatex(0.19, 0.89, "ATLAS")
        atlas_label.SetTextFont(42)
        atlas_label.SetTextSize(0.043)
        atlas_label.DrawLatex(0.35, 0.89, "Internal")

    def _add_cme_lumi_label(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.19, 0.84, "#sqrt{s} = 13 TeV, mc20e")

    def _add_region_label(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.55, 0.89, self.region_name)

    def _add_sample(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.55, 0.84, self.sample)

    def _add_legend(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        cme_lumi_label.DrawLatex(0.19, 0.79, "#color[2]{NominalOnly OR}")
        cme_lumi_label.DrawLatex(0.55, 0.79, "#color[4]{AllSysts}")
        cme_lumi_label.DrawLatex(0.75, 0.79, "#color[3]{Nominal}")

    def _add_systematics(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.035)
        cme_lumi_label.DrawLatex(0.19, 0.69, self.systematics)

    def _add_ks_test_result(self):
        cme_lumi_label = TLatex()
        cme_lumi_label.SetTextAlign()
        cme_lumi_label.SetNDC()
        cme_lumi_label.SetTextFont(42)
        cme_lumi_label.SetTextSize(0.043)
        #cme_lumi_label.DrawLatex(0.19, 0.74, "Kolmogorov-Smirnov p-value: " + str(round(self.ks_test_result,4)))
        cme_lumi_label.DrawLatex(0.19, 0.74, "Chi2 p-value: " + str(round(self.chi2_pvalue,4)))
