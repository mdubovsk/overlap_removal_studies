from plotting_modules.separation_plot import SeparationPlot
from make_plots import region_dict, sample_dict

from ROOT import TFile

from sys import argv
import os

if __name__ == "__main__":
    if len(argv) != 3:
        print("Usage: python compare_nominals.py <root_files_address> <output_address>")
        exit(1)
    root_files_address = argv[1]
    output_address = argv[2]

    all_systs_dir = root_files_address + "/AllSysts/"
    nominal_only_dir = root_files_address + "/NominalOnly/"

    samples = os.listdir(all_systs_dir)
    for sample in samples:
        all_systs_file = TFile(all_systs_dir + sample)
        nominal_only_file = TFile(nominal_only_dir + sample)
        histograms = all_systs_file.Get("NOSYS").GetListOfKeys()
        for histogram in histograms:
            histo_all_systs = all_systs_file.Get("NOSYS/" + histogram.GetName())
            histo_nominal_only = nominal_only_file.Get("NOSYS/" + histogram.GetName())
            region = histogram.GetName().split("_")[-1]
            region_label = region_dict[region]
            dsid = sample_dict[sample]
            separation_plot = SeparationPlot(histo_nominal_only, histo_all_systs, histogram.GetName(), region_label, dsid, "NOSYS")
            separation_plot.set_legend("NominalOnly OR, noiminal var.", "AllSysts OR, noiminal var.")
            separation_plot.save(output_address + "/" + sample_dict[sample] + "_" + histogram.GetName() + ".png")