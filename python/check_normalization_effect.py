from sys import argv
import os
from plotting_modules.comparison_plot import ComparisonPlot
from plotting_modules.set_atlas_style import SetAtlasStyle

from ROOT import TFile

region_dict = {
    "Electron": "e+jets",
    "Muon": "#mu+jets",
}

sample_dict = {
    "ttbar_PowhegHerwig7.root" : "411233",
    "ttbar_PowhegPythiaNominal.root" : "410470",
    "ttbar_sherpa.root" : "700662",
}

def get_syst_to_largest_norm_effect_dict(histo_files_dir : str) -> dict[str, float]:
    """
    Returns a dictionary where the key is the systematic name and the value is the largest normalization effect of that systematic.
    """
    samples = os.listdir(histo_files_dir + "/AllSysts")
    normalization_effects = {}
    for sample in samples:
        file_all_syst     = TFile(histo_files_dir + "/AllSysts/" + sample)
        file_nominal_only = TFile(histo_files_dir + "/NominalOnly/" + sample)
        systematic_list = file_all_syst.GetListOfKeys()
        histogram_names = ["lep_pt_Electron", "lep_pt_Muon"]

        for systematic in systematic_list:
            systematic_name = systematic.GetName()
            if systematic_name not in normalization_effects:
                    normalization_effects[systematic_name] = 0
            for histogram_name in histogram_names:
                integral_nominal_nom_only = file_nominal_only.Get("NOSYS/" + histogram_name).Integral()
                integral_nominal_all_syst = file_all_syst.Get("NOSYS/" + histogram_name).Integral()
                histo_all_syst      = file_all_syst.Get(systematic_name + "/" + histogram_name)
                histo_nominal_only  = file_nominal_only.Get(systematic_name + "/" + histogram_name)
                integral_all_syst     = histo_all_syst.Integral()
                integral_nominal_only = histo_nominal_only.Integral()

                syst_effect_all_syst = integral_all_syst - integral_nominal_all_syst
                syst_effect_nom_only = integral_nominal_only - integral_nominal_nom_only

                normalization_effect = abs((syst_effect_all_syst - syst_effect_nom_only) / integral_all_syst)
                normalization_effects[systematic_name] = max(normalization_effects[systematic_name], normalization_effect)
    return normalization_effects

if __name__ == "__main__":
    #SetAtlasStyle()
    histo_files_dir = argv[1]
    normalization_effects = get_syst_to_largest_norm_effect_dict(histo_files_dir)

    threshold = 0.01
    above_threshold_list = []

    normalization_effects_list = [(value, key) for key, value in normalization_effects.items()]
    normalization_effects_list.sort(key=lambda x:x[0], reverse=True)
    for normalization_effect in normalization_effects_list:
        percent = 100*normalization_effect[0]
        if percent > threshold:
            above_threshold_list.append((normalization_effect))
        print(normalization_effect[1], str(round(percent,5)) + " %")

    print("\n\n\n")
    print("Above threshold:")
    above_threshold_list.sort(key=lambda x:x[1])
    for normalization_effect in above_threshold_list:
        percent = 100*normalization_effect[0]
        print(normalization_effect[1], str(round(percent,5)) + " %")
