from sys import argv

def get_p_values_dict(file_name : str):
    """
    Return dictionary of p-values
    @file_name: text file with p-values
    @return: dictionary with p-values as values and keys as tuples of (dsid, systematics, region, variable)
    """
    p_value_dict = {}
    with open(file_name) as f:
        for line in f:
            line = line.strip()
            # exptract tuple from the parenthesis
            elements = line.split(")")
            tuple_str = elements[0][1:]
            tuple_list = tuple_str.split(",")
            for i in range(len(tuple_list)):
                tuple_list[i] = tuple_list[i].strip("' ")
            dsid = tuple_list[0]
            systematics = tuple_list[1]
            region = tuple_list[2]
            variable = tuple_list[3]
            p_value_string = elements[1].strip()
            p_value = float(p_value_string)
            key_tuple = (dsid, systematics, region, variable)
            p_value_dict[key_tuple] = p_value
    return p_value_dict

def get_plot_name(key_tuple : tuple[str, str, str, str]) -> str:
    dsid = key_tuple[0]
    systematics = key_tuple[1]
    region = key_tuple[2]
    variable = key_tuple[3]
    return f"{dsid}_{systematics}_{variable}_{region}.png"

def get_dict_syst_to_smallest_pvalue_and_file_name(p_value_dict : dict) -> dict[str, float]:
    syst_to_smallest_pvalue = {}
    for key, value in p_value_dict.items():
        syst = key[1]
        if syst not in syst_to_smallest_pvalue:
            syst_to_smallest_pvalue[syst] = (value, get_plot_name(key))
        else:
            if value < syst_to_smallest_pvalue[syst][0]:
                syst_to_smallest_pvalue[syst] = (value, get_plot_name(key))
    return syst_to_smallest_pvalue

if __name__ == "__main__":
    ks_test_threshold = 0.98
    affected_uncertainties = {}
    all_uncertainties = {}
    p_value_dict = get_p_values(argv[1])
    for key, value in p_value_dict.items():
        systematics = key[1]
        if systematics not in all_uncertainties:
            all_uncertainties[systematics] = 1
        if value < ks_test_threshold:
            affected_uncertainties[systematics] = 1

    affected_uncertainties = list(affected_uncertainties.keys())
    affected_uncertainties.sort()

    print("KS test threshold:", ks_test_threshold)
    print("Affected uncertainties:")
    for systematic_name in affected_uncertainties:
        print("\t", systematic_name)

    all_uncertainties = list(all_uncertainties.keys())
    all_uncertainties.sort()
    print("Not affected uncertainties:")
    for systematic_name in all_uncertainties:
        if systematic_name not in affected_uncertainties:
            print("\t", systematic_name)

