#include "../CustomFrame/TopReconstruction.h"
#include "../CustomFrame/PhysicsConstants.h"

#include "FastFrames/Logger.h"

#include <vector>
#include <algorithm>
#include <TLorentzVector.h>


using namespace std;


ROOT::RDF::RNode add_jet_selection_and_or_flag(ROOT::RDF::RNode mainNode, CustomFrame* frame)    {

    auto jet_passed_selection = []( const std::vector<float>& jet_pt,
                                const std::vector<char>& jet_select_or) {
        vector<char> jet_select_or_updated;
        jet_select_or_updated.reserve(jet_pt.size());
        for (unsigned int i_jet = 0; i_jet < jet_pt.size(); ++i_jet) {
            if (jet_pt.at(i_jet) < 25'000) {
                jet_select_or_updated.push_back(1);
            }
            else    {
                jet_select_or_updated.push_back(jet_select_or.at(i_jet));
            }
        }
        return jet_select_or_updated;
    };

    mainNode = mainNode.Define(
                            "jet_select_or_updated", // name of the new column
                            jet_passed_selection, // functor (function that is called)
                            {"jet_pt_NOSYS", "jet_select_or" + frame->m_nosys_suffix}); // what it depends on
    return mainNode;
};


ROOT::RDF::RNode add_passed_selection(ROOT::RDF::RNode mainNode, CustomFrame* frame)    {
    auto passed_selection_1L = []( const std::vector<Lepton>& leptons,
                                float met_met,
                                const std::vector<float>& jet_pt,
                                const std::vector<char>& jet_select_or,
                                const std::vector<char>& jet_DL1dv01_FixedCutBEff_85_select) {
        int n_leptons = 0;
        float lepton_pt = 0;
        for (const auto& lepton : leptons) {
            n_leptons++;
            lepton_pt = lepton.Pt();
        }

        int n_jets = 0;
        int n_bjets = 0;
        for (unsigned int i_jet = 0; i_jet < jet_select_or.size(); ++i_jet) {
            if (jet_pt.at(i_jet) < 25'000) continue;
            if (!jet_select_or.at(i_jet)) continue;

            n_jets++;
            if (jet_DL1dv01_FixedCutBEff_85_select.at(i_jet)) n_bjets++;
        }

        return (n_leptons == 1 && n_jets >= 4 && n_bjets >= 2 && lepton_pt > 27'000 && met_met > 30'000);
    };

    const std::string jet_or_branch = "jet_select_or"s + (frame->m_drop_overlap_removal_for_low_pt_jets ? "_updated" : "") + frame->m_nosys_suffix;
    mainNode = frame->systematicDefine( mainNode,
                                        "reg_4j2b1L_NOSYS", // name of the new column
                                        passed_selection_1L, // functor (function that is called)
                                        {"Leptons_NOSYS", "met_met_NOSYS", "jet_pt_NOSYS", jet_or_branch, "jet_DL1dv01_FixedCutBEff_85_select"}); // what it depends on

    auto passed_selection_2L = []( const std::vector<Lepton>& leptons,
                                float met_met,
                                const std::vector<float>& jet_pt,
                                const std::vector<char>& jet_select_or,
                                const std::vector<char>& jet_DL1dv01_FixedCutBEff_85_select) -> bool {

        if (leptons.size() != 2) return false;

        if (leptons.at(0).Pt() < 27'000) return false;
        if (leptons.at(1).Pt() < 15'000) return false;

        int n_jets = 0;
        int n_bjets = 0;
        for (unsigned int i_jet = 0; i_jet < jet_select_or.size(); ++i_jet) {
            if (jet_pt.at(i_jet) < 25'000) continue;
            if (!jet_select_or.at(i_jet)) continue;

            n_jets++;
            if (jet_DL1dv01_FixedCutBEff_85_select.at(i_jet)) n_bjets++;
        }

        if (n_jets < 2) return false;
        if (n_bjets < 1) return false;

        return met_met > 30'000;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "reg_2L_NOSYS", // name of the new column
                                        passed_selection_2L, // functor (function that is called)
                                        {"Leptons_NOSYS", "met_met_NOSYS", "jet_pt_NOSYS", jet_or_branch, "jet_DL1dv01_FixedCutBEff_85_select"}); // what it depends on

    return mainNode;
};


ROOT::RDF::RNode add_jet_vector(ROOT::RDF::RNode mainNode, CustomFrame* frame)  {
    auto AddJets = [](const std::vector<float>& jet_pt,
                     const std::vector<float>& jet_eta,
                     const std::vector<float>& jet_phi,
                     const std::vector<float>& jet_E,
                     const std::vector<char>& jet_btagged,
                     const std::vector<char>& jet_select_or,
                     const std::vector<char>& jet_select_baseline_jvt
                     ) {
        vector<Jet> jets;
        jets.reserve(jet_pt.size());
        for (unsigned int i_jet = 0; i_jet < jet_pt.size(); ++i_jet) {
            if (jet_select_or.at(i_jet) == 0)           continue;
            if (jet_select_baseline_jvt.at(i_jet) == 0) continue;
            if (jet_pt.at(i_jet) < 25'000)              continue;

            ROOT::Math::PtEtaPhiEVector momentum(jet_pt.at(i_jet), jet_eta.at(i_jet), jet_phi.at(i_jet), jet_E.at(i_jet));
            jets.push_back(Jet(momentum, jet_btagged.at(i_jet)));
        }
        sort(jets.begin(), jets.end(), [](const Jet& a, const Jet& b) {
            return a.Pt() > b.Pt();
        });

        return jets;
    };

    const std::string jet_or_branch = "jet_select_or"s + (frame->m_drop_overlap_removal_for_low_pt_jets ? "_updated" : "") + frame->m_nosys_suffix;
    mainNode = frame->systematicDefine( mainNode,
                                        "jets_NOSYS", // name of the new column
                                        AddJets, // functor (function that is called)
                                        {"jet_pt_NOSYS", "jet_eta", "jet_phi", "jet_e_NOSYS", "jet_DL1dv01_FixedCutBEff_85_select", jet_or_branch, "jet_select_baselineJvt_NOSYS"}); // what it depends on
    return mainNode;
};

ROOT::RDF::RNode add_lepton_vector(ROOT::RDF::RNode mainNode, CustomFrame* frame)   {

    auto AddLeptonVector = [] ( const std::vector<ROOT::Math::PtEtaPhiEVector>& electrons,
                                const std::vector<char>& el_selected,
                                const std::vector<char>& el_select_tight,
                                const std::vector<float>& el_charge,
                                const std::vector<ROOT::Math::PtEtaPhiEVector>& muons,
                                const std::vector<char>& mu_selected,
                                const std::vector<char>& mu_select_tight,
                                const std::vector<float>& mu_charge)    {
        vector<Lepton> leptons;
        if (electrons.size() != el_selected.size()) {
            LOG(ERROR) << "Electrons and electrons selected have different size";
            exit(1);
        }
        if (muons.size() != mu_selected.size()) {
            LOG(ERROR) << "Muons and muons selected have different size";
            exit(1);
        }

        for (unsigned int i_el = 0; i_el < electrons.size(); ++i_el) {
        if (!el_selected.at(i_el) || !el_select_tight.at(i_el)) continue;
            leptons.push_back(Lepton(electrons.at(i_el), el_charge.at(i_el), 11));
        }

        for (unsigned int i_mu = 0; i_mu < muons.size(); ++i_mu) {
            if (!mu_selected.at(i_mu) || !mu_select_tight.at(i_mu)) continue;
            leptons.push_back(Lepton(muons.at(i_mu), mu_charge.at(i_mu), 13));
        }

        sort(leptons.begin(), leptons.end(), [](const Lepton& a, const Lepton& b) {
            return a.Pt() > b.Pt();
        });
        return leptons;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "Leptons_NOSYS", // name of the new column
                                        AddLeptonVector, // functor (function that is called)
                                        {   "el_TLV_NOSYS", "el_select_or" + frame->m_nosys_suffix, "el_select_tight_NOSYS", "el_charge",
                                            "mu_TLV_NOSYS", "mu_select_or" + frame->m_nosys_suffix, "mu_select_tight_NOSYS", "mu_charge"}); // what it depends on

    return mainNode;
};

ROOT::RDF::RNode add_bjets_indices(ROOT::RDF::RNode mainNode, CustomFrame* frame)   {
    auto get_bjet_indices = [](const std::vector<Jet>& jets) {

        vector<int> bjet_indices;
        for (unsigned int i_jet = 0; i_jet < jets.size(); ++i_jet) {
            if (jets.at(i_jet).is_btagged()) continue;
            bjet_indices.push_back(i_jet);
        }
        return bjet_indices;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "bjet_indices_NOSYS", // name of the new column
                                        get_bjet_indices, // functor (function that is called)
                                        {"jets_NOSYS"}); // what it depends on
    return mainNode;
};

ROOT::RDF::RNode add_bjet_indices_and_neutrino(ROOT::RDF::RNode mainNode, CustomFrame* frame)   {
    //firstly add all 3 variables as tuple (yeah, the RDataFrame interface is not ideal ...)
    auto AddNuTLVAndIndicesTuple = [](  float met_met, float met_phi,
                                        const vector<Lepton> &leptons,
                                        const std::vector<Jet> &jets,
                                        const std::vector<int> &bjet_indices)   {
        if (leptons.size() == 0) return make_tuple(ROOT::Math::PtEtaPhiEVector(0,0,0,0), -1, -1);
        const ROOT::Math::PtEtaPhiEVector &lepton = leptons.at(0).four_momentum();
        const ROOT::Math::PtEtaPhiEVector met(met_met, 0, met_phi, met_met);

        vector<ROOT::Math::PtEtaPhiEVector> jet_fourmomenta;
        for (const Jet& jet : jets) {
            jet_fourmomenta.push_back(jet.four_momentum());
        }

        return fit_neutrino_momentum_lep_had_top_indices(met, lepton, jet_fourmomenta, bjet_indices);
    };
    mainNode = frame->systematicDefine(     mainNode,
                                            "neutrino_momentum_lep_had_top_indices_tuple_NOSYS", // name of the new column
                                            AddNuTLVAndIndicesTuple, // functor (function that is called)
                                            {"met_met_NOSYS", "met_phi_NOSYS", "Leptons_NOSYS", "jets_NOSYS", "bjet_indices_NOSYS"}); // what it depends on

    // now make it 3 separate columns
    mainNode = frame->systematicDefine(     mainNode,
                                            "neutrino_TLV_NOSYS", // name of the new column
                                            [](const tuple<ROOT::Math::PtEtaPhiEVector, int, int> &t) {return get<0>(t);}, // functor (function that is called)
                                            {"neutrino_momentum_lep_had_top_indices_tuple_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine(     mainNode,
                                            "index_leptonic_b_NOSYS", // name of the new column
                                            [](const tuple<ROOT::Math::PtEtaPhiEVector, int, int> &t) {return get<1>(t);}, // functor (function that is called)
                                            {"neutrino_momentum_lep_had_top_indices_tuple_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine(     mainNode,
                                            "index_hadronic_b_NOSYS", // name of the new column
                                            [](const tuple<ROOT::Math::PtEtaPhiEVector, int, int> &t) {return get<2>(t);}, // functor (function that is called)
                                            {"neutrino_momentum_lep_had_top_indices_tuple_NOSYS"}); // what it depends on

    return mainNode;
};


ROOT::RDF::RNode add_leptonic_top(ROOT::RDF::RNode mainNode, CustomFrame* frame)   {
    auto  LeptonicTop = []( const std::vector<Jet>& jets,
                            const std::vector<Lepton> &leptons,
                            const ROOT::Math::PtEtaPhiEVector& neutrino,
                            const int index_leptonic_b) {
        if (index_leptonic_b < 0 || leptons.size() == 0) return ROOT::Math::PtEtaPhiEVector(0,0,0,0);
        ROOT::Math::PtEtaPhiEVector lepton = leptons.at(0).four_momentum();
        ROOT::Math::PtEtaPhiEVector bjet = jets.at(index_leptonic_b).four_momentum();
        ROOT::Math::PtEtaPhiEVector top_lep = lepton + neutrino + bjet;

        return top_lep;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "top_lep_TLV_NOSYS", // name of the new column
                                        LeptonicTop, // functor (function that is called)
                                        {"jets_NOSYS", "Leptons_NOSYS", "neutrino_TLV_NOSYS", "index_leptonic_b_NOSYS"}); // what it depends on
    return mainNode;
};

ROOT::RDF::RNode add_hadronic_top(ROOT::RDF::RNode mainNode, CustomFrame* frame)   {
    auto  HadronicTop = []( const std::vector<Jet>& jets,
                            const int index_leptonic_b,
                            const int index_hadronic_b) {
        if (index_leptonic_b < 0)   return ROOT::Math::PtEtaPhiEVector(0,0,0,0);
        if (jets.size() < 4)        return ROOT::Math::PtEtaPhiEVector(0,0,0,0);

        ROOT::Math::PtEtaPhiEVector top_had = jets.at(index_hadronic_b).four_momentum();
        int used_light_jets = 0;
        for (unsigned int i_jet = 0; i_jet < jets.size(); ++i_jet) {
            if (int(i_jet) == index_leptonic_b) continue;
            if (int(i_jet) == index_hadronic_b) continue;
            top_had += jets.at(i_jet).four_momentum();
            used_light_jets++;
            if (used_light_jets == 2) break;
        }
        return top_had;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "top_had_TLV_NOSYS", // name of the new column
                                        HadronicTop, // functor (function that is called)
                                        {"jets_NOSYS", "index_leptonic_b_NOSYS", "index_hadronic_b_NOSYS"}); // what it depends on
    return mainNode;
};

ROOT::RDF::RNode add_output_top_related_variables(ROOT::RDF::RNode mainNode, CustomFrame* frame)    {


    mainNode = frame->systematicDefine( mainNode,
                                        "top_lep_pt_GeV_NOSYS", // name of the new column
                                        [](const ROOT::Math::PtEtaPhiEVector& top_lep)  {return 0.001*top_lep.Pt();}, // functor (function that is called)
                                        {"top_lep_TLV_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "top_lep_m_GeV_NOSYS", // name of the new column
                                        [](const ROOT::Math::PtEtaPhiEVector& top_lep)  {return 0.001*top_lep.M();}, // functor (function that is called)
                                        {"top_lep_TLV_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "top_had_pt_GeV_NOSYS", // name of the new column
                                        [](const ROOT::Math::PtEtaPhiEVector& top_had)  {return 0.001*top_had.Pt();}, // functor (function that is called)
                                        {"top_had_TLV_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "top_had_m_GeV_NOSYS", // name of the new column
                                        [](const ROOT::Math::PtEtaPhiEVector& top_had)  {return 0.001*top_had.M();}, // functor (function that is called)
                                        {"top_had_TLV_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "ttbar_m_GeV_NOSYS", // name of the new column
                                        [](const ROOT::Math::PtEtaPhiEVector& top_had, const ROOT::Math::PtEtaPhiEVector& top_lep)  {return 0.001*(top_had+top_lep).M();}, // functor (function that is called)
                                        {"top_had_TLV_NOSYS", "top_lep_TLV_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "ttbar_pt_GeV_NOSYS", // name of the new column
                                        [](const ROOT::Math::PtEtaPhiEVector& top_had, const ROOT::Math::PtEtaPhiEVector& top_lep)  {return 0.001*(top_had+top_lep).Pt();}, // functor (function that is called)
                                        {"top_had_TLV_NOSYS", "top_lep_TLV_NOSYS"}); // what it depends on


    mainNode = frame->systematicDefine( mainNode,
                                        "index_bjet_1_NOSYS", // name of the new column
                                        [](const std::vector<int> &bjet_indices)  {return bjet_indices.size() >= 1 ? bjet_indices[0] : -1;}, // functor (function that is called)
                                        {"bjet_indices_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "index_bjet_2_NOSYS", // name of the new column
                                        [](const std::vector<int> &bjet_indices)  {return bjet_indices.size() >= 2 ? bjet_indices[1] : -1;}, // functor (function that is called)
                                        {"bjet_indices_NOSYS"}); // what it depends on
    return mainNode;
};

ROOT::RDF::RNode add_basic_kinematic_variables(ROOT::RDF::RNode mainNode, CustomFrame* frame)   {
    mainNode = frame->systematicDefine( mainNode,
                                        "n_jets_NOSYS", // name of the new column
                                        [](const std::vector<Jet> &jets)-> int {return jets.size();}, // functor (function that is called)
                                        {"jets_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "n_bjets_NOSYS", // name of the new column
                                        [](const std::vector<Jet> &jets) -> int {
                                            int n_bjets = 0;
                                            for (const Jet& jet : jets) {
                                                if (jet.is_btagged()) n_bjets++;
                                            }
                                            return n_bjets;
                                        }, // functor (function that is called)
                                        {"jets_NOSYS"}); // what it depends on


    auto add_jet_property_variable = [frame](ROOT::RDF::RNode node, int jet_index, const std::string &property_name, auto method){
        const std::string column_name = "jet_" + std::to_string(jet_index) + "_" + property_name + "_NOSYS";
        node = frame->systematicDefine( node,
                                        column_name, // name of the new column
                                        method, // functor
                                        {"jets_NOSYS"}); // what it depends on
        return node;
    };

    auto jet_pt_GeV = [](unsigned int index) {
        return [index](const std::vector<Jet> &jets) -> float  {return jets.size() > index ? 0.001*jets[index].Pt() : -1;};
    };

    mainNode = add_jet_property_variable(mainNode, 1, "pt_GeV", jet_pt_GeV(0));
    mainNode = add_jet_property_variable(mainNode, 2, "pt_GeV", jet_pt_GeV(1));
    mainNode = add_jet_property_variable(mainNode, 3, "pt_GeV", jet_pt_GeV(2));
    mainNode = add_jet_property_variable(mainNode, 4, "pt_GeV", jet_pt_GeV(3));

    auto jet_eta = [](unsigned int index) {
        return [index](const std::vector<Jet> &jets) -> float  {return jets.size() > index ? jets[index].Eta() : -10;};
    };

    mainNode = add_jet_property_variable(mainNode, 1, "eta", jet_eta(0));
    mainNode = add_jet_property_variable(mainNode, 2, "eta", jet_eta(1));
    mainNode = add_jet_property_variable(mainNode, 3, "eta", jet_eta(2));
    mainNode = add_jet_property_variable(mainNode, 4, "eta", jet_eta(3));


    mainNode = frame->systematicDefine( mainNode,
                                        "lep_pt_GeV_NOSYS",
                                        [](const std::vector<Lepton> &leptons) -> float  {return leptons.size() >= 1 ? 0.001*leptons[0].Pt() : -1;}, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "lep_eta_NOSYS",
                                        [](const std::vector<Lepton> &leptons) -> float  {return leptons.size() >= 1 ? leptons[0].Eta() : -1;}, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on


    mainNode = frame->systematicDefine( mainNode,
                                        "lep2_pt_GeV_NOSYS",
                                        [](const std::vector<Lepton> &leptons) -> float  {return leptons.size() >= 2 ? 0.001*leptons[1].Pt() : -1;}, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "lep2_eta_NOSYS",
                                        [](const std::vector<Lepton> &leptons) -> float  {return leptons.size() >= 2 ? leptons[1].Eta() : -1;}, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "lepton_pdgId_NOSYS",
                                        [](const std::vector<Lepton> &leptons)  {return leptons.size() >= 1 ? leptons[0].pdgId() : -1;}, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    mainNode = frame->systematicDefine( mainNode,
                                        "met_met_GeV_NOSYS",
                                        [](float met_met)  {return 0.001*met_met;}, // functor (function that is called)
                                        {"met_met_NOSYS"}); // what it depends on


    return mainNode;
};

ROOT::RDF::RNode add_2L_variables(ROOT::RDF::RNode mainNode, CustomFrame* frame)    {
    auto addMll = [](const std::vector<Lepton> &leptons) -> float  {
        if (leptons.size() < 2) return -1;
        return 0.001*(leptons[0].four_momentum() + leptons[1].four_momentum()).M();
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "mll_GeV_NOSYS", // name of the new column
                                        addMll, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    auto addPtll = [](const std::vector<Lepton> &leptons) -> float  {
        if (leptons.size() < 2) return -1;
        return 0.001*(leptons[0].four_momentum() + leptons[1].four_momentum()).Pt();
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "ptll_GeV_NOSYS", // name of the new column
                                        addPtll, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    auto nElectrons = [](const std::vector<Lepton> &leptons) -> int  {
        int n = 0;
        for (const Lepton &lep : leptons) {
            if (lep.is_electron()) n++;
        }
        return n;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "nElectrons_NOSYS", // name of the new column
                                        nElectrons, // functor (function that is called)
                                        {"Leptons_NOSYS"}); // what it depends on

    auto DeltaR = [](const ROOT::Math::PtEtaPhiEVector &x1, const ROOT::Math::PtEtaPhiEVector &x2) -> float  {
        float dphi = x1.Phi() - x2.Phi();
        if (dphi > TMath::Pi()) dphi -= 2*TMath::Pi();
        dphi = fabs(dphi);
        return sqrt(pow(x1.Eta() - x2.Eta(), 2) + pow(dphi, 2));
    };

    auto minDrLepBjet = [DeltaR](const std::vector<Lepton> &leptons, const std::vector<Jet> &jets) -> float  {
        if (leptons.size() == 0) return -1;
        if (jets.size() == 0) return -1;
        float minDr = 999;
        for (const Lepton &lep : leptons) {
            for (const Jet &jet : jets) {
                if (jet.is_btagged()) {
                    const float dr = DeltaR(lep.four_momentum(), jet.four_momentum());
                    if (dr < minDr) minDr = dr;
                }
            }
        }
        return minDr;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "minDrLepBjet_NOSYS", // name of the new column
                                        minDrLepBjet, // functor (function that is called)
                                        {"Leptons_NOSYS", "jets_NOSYS"}); // what it depends on

    auto minDrLepJet = [DeltaR](const std::vector<Lepton> &leptons, const std::vector<Jet> &jets) -> float  {
        if (leptons.size() == 0) return -1;
        if (jets.size() == 0) return -1;
        float minDr = 999;
        for (const Lepton &lep : leptons) {
            for (const Jet &jet : jets) {
                const float dr = DeltaR(lep.four_momentum(), jet.four_momentum());
                if (dr < minDr) minDr = dr;
            }
        }
        return minDr;
    };

    mainNode = frame->systematicDefine( mainNode,
                                        "minDrLepJet_NOSYS", // name of the new column
                                        minDrLepJet, // functor (function that is called)
                                        {"Leptons_NOSYS", "jets_NOSYS"}); // what it depends on

    return mainNode;
};

std::vector<ROOT::Math::PtEtaPhiEVector>  get_neutrino_candidates(const ROOT::Math::PtEtaPhiEVector &met, const ROOT::Math::PtEtaPhiEVector &lep)  {
    vector<float> v_pz;
    double nu_px = met.Px();
    double nu_py = met.Py();
    double nu_pt;
    double mass_term = PhysicsConstants::w_mass*PhysicsConstants::w_mass - lep.M()*lep.M();
    double alpha = mass_term + 2*( nu_px*lep.Px() + nu_py*lep.Py() );
    double a = lep.Pz()*lep.Pz() - lep.E()*lep.E();
    double b = alpha * lep.Pz();
    double c = alpha*alpha/4 - lep.E()*lep.E()*( nu_px*nu_px + nu_py*nu_py );
    double discriminant = b*b - 4*a*c;

    if (discriminant < 0) {
        // solve algebraically for when b^2 = 4ac -- by definition the point at which the discriminant becomes non-negative
        // solution below then corresponds to the quadratic equation A0*pTnu^2 + B0*pTnu + C0 = 0
        double A0 = lep.E()*lep.E() - lep.Pz()*lep.Pz() - pow(lep.Px()*TMath::Cos(met.Phi()) + lep.Py()*TMath::Sin(met.Phi()),2);
        double B0 = -mass_term*(lep.Px()*TMath::Cos(met.Phi()) + lep.Py()*TMath::Sin(met.Phi()));
        double C0 = -mass_term*mass_term/4.;
        nu_pt = ( -B0 + sqrt(B0*B0 - 4.*A0*C0) )/(2.*A0); // positive root only for neutrino pT
        nu_px = nu_pt*TMath::Cos(met.Phi());
        nu_py = nu_pt*TMath::Sin(met.Phi());
        alpha = mass_term + 2.*( nu_px*lep.Px() + nu_py*lep.Py() );
        b = alpha * lep.Pz();
        c = alpha*alpha/4 - lep.E()*lep.E()*nu_pt*nu_pt;
        discriminant = 0;
    }

    if (discriminant < 0) v_pz.push_back( 0. );
    else {
        v_pz.push_back( ( -b + sqrt(discriminant) ) / ( 2*a ) );
        v_pz.push_back( ( -b - sqrt(discriminant) ) / ( 2*a ) );
    }
    vector<ROOT::Math::PtEtaPhiEVector> candidates_vec_nu;
    for( float nu_pz: v_pz ) {
        ROOT::Math::PtEtaPhiEVector vec_nu;
        vec_nu.SetPxPyPzE( nu_px, nu_py, nu_pz, sqrt( nu_px*nu_px + nu_py*nu_py + nu_pz*nu_pz ) );
        candidates_vec_nu.push_back( vec_nu );
    }
    return candidates_vec_nu;
}


std::tuple<ROOT::Math::PtEtaPhiEVector, int, int> fit_neutrino_momentum_lep_had_top_indices(
                            const ROOT::Math::PtEtaPhiEVector &met,
                            const ROOT::Math::PtEtaPhiEVector &lep,
                            const std::vector<ROOT::Math::PtEtaPhiEVector> &jets,
                            const std::vector<int> &bjet_indices)  {

    const tuple<ROOT::Math::PtEtaPhiEVector, int, int> null_tuple(ROOT::Math::PtEtaPhiEVector(0,0,0,0), -1, -1);

    if (bjet_indices.size() < 2)    return null_tuple;

    vector<ROOT::Math::PtEtaPhiEVector> candidates_vec_nu = get_neutrino_candidates(met, lep);
    if (candidates_vec_nu.size() == 0) return null_tuple;

    int index_leptonic_b = -1;
    int index_neutrino   = -1;
    float best_top_mass = 1e12;
    for (unsigned int i_nu = 0; i_nu < candidates_vec_nu.size(); ++i_nu) {
        for (unsigned int i_b = 0; i_b < 2; ++i_b) {
            const int index_b = bjet_indices.at(i_b);
            const ROOT::Math::PtEtaPhiEVector &bjet = jets.at(index_b);
            const ROOT::Math::PtEtaPhiEVector top_lep = lep + candidates_vec_nu.at(i_nu) + bjet;
            const float top_mass = top_lep.M();
            if (fabs(top_mass - PhysicsConstants::top_mass) < fabs(best_top_mass - PhysicsConstants::top_mass)) {
                best_top_mass = top_mass;
                index_neutrino = i_nu;
                index_leptonic_b = index_b;
            }
        }
    }

    // find hadronic b-jet
    int index_hadronic_b = -1;
    for (unsigned int i_b = 0; i_b < 2; ++i_b) {
        const int index_b = bjet_indices.at(i_b);
        if (index_b == index_leptonic_b) {
            continue;
        }
        else {
            index_hadronic_b = index_b;
            break;
        }
    }

    if (index_leptonic_b < 0 || index_hadronic_b < 0) return null_tuple;

    return make_tuple(candidates_vec_nu.at(index_neutrino), index_leptonic_b, index_hadronic_b);
};
