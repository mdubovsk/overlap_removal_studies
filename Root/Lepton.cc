#include "../CustomFrame/Lepton.h"


Lepton::Lepton(const ROOT::Math::PtEtaPhiEVector &four_momentum, int charge, int pdgId) {
    m_four_momentum = four_momentum;
    m_charge = charge;
    m_pdgId = pdgId;
};

const ROOT::Math::PtEtaPhiEVector& Lepton::four_momentum() const    {
    return m_four_momentum;
};

void Lepton::set_four_momentum(const ROOT::Math::PtEtaPhiEVector &four_momentum)    {
    m_four_momentum = four_momentum;
};

float Lepton::Pt() const    {
    return m_four_momentum.Pt();
};

float Lepton::Eta() const   {
    return m_four_momentum.Eta();
};

float Lepton::Phi() const   {
    return m_four_momentum.Phi();
};

float Lepton::E() const {
    return m_four_momentum.E();
}

int  Lepton::charge() const {
    return m_charge;
};

void Lepton::set_charge(int charge) {
    m_charge = charge;
};

int  Lepton::pdgId() const  {
    return m_pdgId;
};

void Lepton::set_pdgId(int pdgId)   {
    m_pdgId = pdgId;
};

bool Lepton::is_electron() const    {
    return abs(m_pdgId) == 11;
};

bool Lepton::is_muon() const    {
    return abs(m_pdgId) == 13;
};

