#include "CustomFrame/CustomFrame.h"
#include "CustomFrame/Lepton.h"
#include "CustomFrame/TopReconstruction.h"

#include "FastFrames/DefineHelpers.h"
#include "FastFrames/UniqueSampleID.h"

#include<algorithm>

using namespace std;

void CustomFrame::init() {
  MainFrame::init();
  m_drop_overlap_removal_for_low_pt_jets = m_config->customOptions().getOption<bool>("drop_overlap_removal_for_low_pt_jets", false);
};

ROOT::RDF::RNode CustomFrame::defineVariables(ROOT::RDF::RNode mainNode,
                                              const UniqueSampleID& /*id*/) {

  // You can also use the UniqueSampleID object to apply a custom defione
  // based on the sample:
  //   id.dsid() returns sample DSID
  //   id.campaign() returns sample campaign
  //   id.simulation() return simulation flavour
  // You can use it in your functions to apply only per sample define


  auto column_names = mainNode.GetColumnNames();
  bool nominal_only_ntuples = false;
  for (auto& name : column_names) {
    if (name == "el_select_or") {
      nominal_only_ntuples = true;
      break;
    }
  }
  if (nominal_only_ntuples) {
    m_nosys_suffix = "";
  }
  else {
    m_nosys_suffix = "_NOSYS";
  }

  if (m_drop_overlap_removal_for_low_pt_jets) {
    mainNode = add_jet_selection_and_or_flag(mainNode, this);
  }

  mainNode = add_lepton_vector(mainNode, this);
  mainNode = add_passed_selection(mainNode, this);
  mainNode = add_jet_vector(mainNode, this);
  mainNode = add_bjets_indices(mainNode, this);
  mainNode = add_bjet_indices_and_neutrino(mainNode, this);
  mainNode = add_leptonic_top(mainNode, this);
  mainNode = add_hadronic_top(mainNode, this);
  mainNode = add_output_top_related_variables(mainNode, this);
  mainNode = add_basic_kinematic_variables(mainNode, this);
  mainNode = add_2L_variables(mainNode, this);

  return mainNode;
}

ROOT::RDF::RNode CustomFrame::defineVariablesNtuple(ROOT::RDF::RNode mainNode,
                                                    const UniqueSampleID& /*id*/) {

  auto LeadingElectronPtTight = [](const std::vector<ROOT::Math::PtEtaPhiEVector>& electrons,
                                   const std::vector<char>& passed,
                                   const std::vector<char>& passedTight) {

    auto sortedElectrons = DefineHelpers::sortedPassedVector(electrons, passed, passedTight);
    if (sortedElectrons.empty()) return -1.;
    return sortedElectrons.at(0).pt();
  };

  LOG(INFO) << "Adding variable: leading_tight_electron_pt_NOSYS\n";
  mainNode = MainFrame::systematicDefine(mainNode,
                                         "leading_tight_electron_pt_NOSYS", // name of the new column
                                         LeadingElectronPtTight, // functor (function that is called)
                                         {"el_TLV_NOSYS", "el_select_or", "el_select_tight_NOSYS"}); // what it depends on

  return mainNode;
}

bool CustomFrame::passes4Jets50GeV1Btag(const std::vector<ROOT::Math::PtEtaPhiEVector>& fourVec,
                                        const std::vector<char>& selected,
                                        const std::vector<char>& btagged) const {

  // we only want to select events where we have at least 4 jets, each with 50 GeV and at least one btag

  // first get the ordered indices of the selected jets (that passed the current selection)

  std::vector<std::size_t> sortedIndices = DefineHelpers::sortedPassedIndices(fourVec, selected);

  // now get the ordered vectors for four momentum and btagging
  std::vector<ROOT::Math::PtEtaPhiEVector> sortedTLV = DefineHelpers::vectorFromIndices(fourVec, sortedIndices);
  std::vector<char> sortedTag = DefineHelpers::vectorFromIndices(btagged, sortedIndices);

  // now we can loop over them and count number of correct indices
  std::size_t nJets(0);
  std::size_t nTags(0);
  for (std::size_t i = 0; i < sortedTLV.size(); ++i) {
    if (sortedTLV.at(i).pt() < 50e3) continue;

    nJets++;
    if (sortedTag.at(i)) nTags++;
  }

  return (nJets >= 4) && (nTags > 0);
}
