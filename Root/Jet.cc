#include "../CustomFrame/Jet.h"


Jet::Jet(const ROOT::Math::PtEtaPhiEVector &four_momentum, bool is_btagged)  {
    set_four_momentum(four_momentum);
    set_is_btagged(is_btagged);
};

const ROOT::Math::PtEtaPhiEVector &Jet::four_momentum() const    {
    return m_four_momentum;
};

void Jet::set_four_momentum(const ROOT::Math::PtEtaPhiEVector &four_momentum)    {
    m_four_momentum = four_momentum;
};

float Jet::Pt() const    {
    return four_momentum().Pt();
};

float Jet::Eta() const   {
    return four_momentum().Eta();
};

float Jet::Phi() const   {
    return four_momentum().Phi();
};

float Jet::E() const     {
    return four_momentum().E();
};

bool Jet::is_btagged() const {
    return m_is_btagged;
};

void Jet::set_is_btagged(bool is_btagged)    {
    m_is_btagged = is_btagged;
};
