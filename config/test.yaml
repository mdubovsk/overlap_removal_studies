general:
  debug_level: INFO
  input_filelist_path: "/home/dubovsky/Analysis/ROOT_Files/OR_removal_studies/NominalOnly/metadata/filelist.txt"
  input_sumweights_path: "/home/dubovsky/Analysis/ROOT_Files/OR_removal_studies/NominalOnly/metadata/sum_of_weights.txt"
  output_path_histograms: "/home/dubovsky/Analysis/ROOT_Files/Reprocessed/20240116_OR/NominalOnly/"
  output_path_ntuples: "/home/dubovsky/Analysis/ROOT_Files/Reprocessed/20240116_OR/NominalOnly/"
  default_sumweights: "NOSYS"
  default_event_weights: "weight_mc_NOSYS * weight_pileup_NOSYS * weight_jvt_effSF_NOSYS * weight_btagSF_DL1dv01_FixedCutBEff_85_NOSYS * globalTriggerEffSF_NOSYS * weight_leptonSF_tight_NOSYS"
  default_reco_tree_name: "reco"
  xsection_files: ["data/XSection-MC16-13TeV.data"]
  custom_frame_name: "CustomFrame"
  automatic_systematics: True
  nominal_only: False
  create_tlorentz_vectors_for: ["jet", "el", "mu"]
  number_of_cpus: 8
  luminosity:
    mc23c: 1
  define_custom_columns:
    - name: "jet_pt_GeV_NOSYS"
      definition: "jet_pt_NOSYS/1e3"
    - name: "el_pt_GeV_NOSYS"
      definition: "el_pt_NOSYS/1e3"

regions:
  - name: "Electron"
    selection: "reg_4j2b1L_NOSYS && (lepton_pdgId_NOSYS == 11)"
    variables:
      - name: "top_lep_pt"
        title : " ; p_{T}^{top-lep} [GeV]; Events"
        definition: "top_lep_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 500
          number_of_bins: 20
      - name: "top_had_pt"
        title : " ; p_{T}^{top-had} [GeV]; Events"
        definition: "top_had_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 500
          number_of_bins: 20
      - name: "ttbar_pt"
        title : " ; p_{T}^{t#bar{t}} [GeV]; Events"
        definition: "ttbar_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 700
          number_of_bins: 35

      - name: "top_lep_m"
        title : " ; m^{top-lep} [GeV]; Events"
        definition: "top_lep_m_GeV_NOSYS"
        binning:
          min: 50
          max: 350
          number_of_bins: 30
      - name: "top_had_m"
        title : " ; m^{top-had} [GeV]; Events"
        definition: "top_had_m_GeV_NOSYS"
        binning:
          min: 50
          max: 500
          number_of_bins: 45
      - name: "ttbar_m"
        title : " ; m^{t#bar{t}} [GeV]; Events"
        definition: "ttbar_m_GeV_NOSYS"
        binning:
          min: 200
          max: 1500
          number_of_bins: 39

      # basic kinematics
      - name: "n_jets"
        title : " ; N_{jets}; Events"
        definition: "n_jets_NOSYS"
        binning:
          min: 2.5
          max: 10.5
          number_of_bins: 8
      - name: "n_bjets"
        title : " ; N_{b-jets}; Events"
        definition: "n_bjets_NOSYS"
        binning:
          min: 0.5
          max: 5.5
          number_of_bins: 5
      - name: "jet_1_pt"
        title : " ; p_{T}^{1st jet} [GeV]; Events"
        definition: "jet_1_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_2_pt"
        title : " ; p_{T}^{2nd jet} [GeV]; Events"
        definition: "jet_2_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_3_pt"
        title : " ; p_{T}^{3rd jet} [GeV]; Events"
        definition: "jet_3_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_4_pt"
        title : " ; p_{T}^{4th jet} [GeV]; Events"
        definition: "jet_4_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_1_eta"
        title : " ; #eta^{1st jet}; Events"
        definition: "jet_1_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "jet_2_eta"
        title : " ; #eta^{2nd jet}; Events"
        definition: "jet_2_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "jet_3_eta"
        title : " ; #eta^{3rd jet}; Events"
        definition: "jet_3_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "jet_4_eta"
        title : " ; #eta^{4th jet}; Events"
        definition: "jet_4_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27

      - name: "lep_pt"
        title : " ; p_{T}^{lep.} [GeV]; Events"
        definition: "lep_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 250
          number_of_bins: 25
      - name: "lep_eta"
        title : " ; #eta^{lep.}; Events"
        definition: "lep_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "met_met"
        title : " ; E_{T}^{miss} [GeV]; Events"
        definition: "met_met_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 30

  - name: "Muon"
    selection: "reg_4j2b1L_NOSYS && (lepton_pdgId_NOSYS == 13)"
    variables:
      - name: "top_lep_pt"
        title : " ; p_{T}^{top-lep} [GeV]; Events"
        definition: "top_lep_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 500
          number_of_bins: 20
      - name: "top_had_pt"
        title : " ; p_{T}^{top-had} [GeV]; Events"
        definition: "top_had_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 500
          number_of_bins: 20
      - name: "ttbar_pt"
        title : " ; p_{T}^{t#bar{t}} [GeV]; Events"
        definition: "ttbar_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 700
          number_of_bins: 35

      - name: "top_lep_m"
        title : " ; m^{top-lep} [GeV]; Events"
        definition: "top_lep_m_GeV_NOSYS"
        binning:
          min: 50
          max: 350
          number_of_bins: 30
      - name: "top_had_m"
        title : " ; m^{top-had} [GeV]; Events"
        definition: "top_had_m_GeV_NOSYS"
        binning:
          min: 50
          max: 500
          number_of_bins: 45
      - name: "ttbar_m"
        title : " ; m^{t#bar{t}} [GeV]; Events"
        definition: "ttbar_m_GeV_NOSYS"
        binning:
          min: 200
          max: 1500
          number_of_bins: 39

      # basic kinematics
      - name: "n_jets"
        title : " ; N_{jets}; Events"
        definition: "n_jets_NOSYS"
        binning:
          min: 2.5
          max: 10.5
          number_of_bins: 8
      - name: "n_bjets"
        title : " ; N_{b-jets}; Events"
        definition: "n_bjets_NOSYS"
        binning:
          min: 0.5
          max: 5.5
          number_of_bins: 5
      - name: "jet_1_pt"
        title : " ; p_{T}^{1st jet} [GeV]; Events"
        definition: "jet_1_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_2_pt"
        title : " ; p_{T}^{2nd jet} [GeV]; Events"
        definition: "jet_2_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_3_pt"
        title : " ; p_{T}^{3rd jet} [GeV]; Events"
        definition: "jet_3_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_4_pt"
        title : " ; p_{T}^{4th jet} [GeV]; Events"
        definition: "jet_4_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 20
      - name: "jet_1_eta"
        title : " ; #eta^{1st jet}; Events"
        definition: "jet_1_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "jet_2_eta"
        title : " ; #eta^{2nd jet}; Events"
        definition: "jet_2_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "jet_3_eta"
        title : " ; #eta^{3rd jet}; Events"
        definition: "jet_3_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "jet_4_eta"
        title : " ; #eta^{4th jet}; Events"
        definition: "jet_4_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27

      - name: "lep_pt"
        title : " ; p_{T}^{lep.} [GeV]; Events"
        definition: "lep_pt_GeV_NOSYS"
        binning:
          min: 0
          max: 250
          number_of_bins: 25
      - name: "lep_eta"
        title : " ; #eta^{lep.}; Events"
        definition: "lep_eta_NOSYS"
        binning:
          min: -2.7
          max: 2.7
          number_of_bins: 27
      - name: "met_met"
        title : " ; E_{T}^{miss} [GeV]; Events"
        definition: "met_met_GeV_NOSYS"
        binning:
          min: 0
          max: 300
          number_of_bins: 30




samples:
  - name: "ttbar_PowhegHerwig7"
    dsids: [411233]
    campaigns: ["mc20e"]
    simulation_type: "fastsim"
