#include "../CustomFrame/CustomFrame.h"
#include "../CustomFrame/Jet.h"
#include "../CustomFrame/Lepton.h"

#include "Math/Vector4D.h"
#include "ROOT/RDataFrame.hxx"
#include "TClass.h"

#include <vector>
#include <tuple>


ROOT::RDF::RNode add_jet_selection_and_or_flag(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_passed_selection(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_jet_vector(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_lepton_vector(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_bjets_indices(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_bjet_indices_and_neutrino(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_leptonic_top(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_hadronic_top(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_output_top_related_variables(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_basic_kinematic_variables(ROOT::RDF::RNode mainNode, CustomFrame* frame);

ROOT::RDF::RNode add_2L_variables(ROOT::RDF::RNode mainNode, CustomFrame* frame);


std::vector<ROOT::Math::PtEtaPhiEVector>  get_neutrino_candidates(const ROOT::Math::PtEtaPhiEVector &met, const ROOT::Math::PtEtaPhiEVector &lep);

std::tuple<ROOT::Math::PtEtaPhiEVector, int, int> fit_neutrino_momentum_lep_had_top_indices(
                            const ROOT::Math::PtEtaPhiEVector &met,
                            const ROOT::Math::PtEtaPhiEVector &lep,
                            const std::vector<ROOT::Math::PtEtaPhiEVector> &jets,
                            const std::vector<int> &bjet_indices);