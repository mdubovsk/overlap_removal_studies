#pragma once

#include "FastFrames/MainFrame.h"

#include "FastFrames/ConfigSetting.h"
#include "FastFrames/Truth.h"
#include "FastFrames/UniqueSampleID.h"

#include "Math/Vector4D.h"
#include "ROOT/RDataFrame.hxx"
#include "TClass.h"

#include <memory>

using TLV = ROOT::Math::PtEtaPhiEVector;

class UniqueSampleID;

class CustomFrame : public MainFrame {
public:

  explicit CustomFrame() = default;

  virtual ~CustomFrame() = default;

  virtual void init() override final;

  virtual ROOT::RDF::RNode defineVariables(ROOT::RDF::RNode mainNode,
                                           const UniqueSampleID& id) override final;

  virtual ROOT::RDF::RNode defineVariablesNtuple(ROOT::RDF::RNode mainNode,
                                                 const UniqueSampleID& id) override final;

  std::string m_nosys_suffix = "";
  bool m_drop_overlap_removal_for_low_pt_jets = false;

private:

  bool passes4Jets50GeV1Btag(const std::vector<ROOT::Math::PtEtaPhiEVector>& fourVec,
                             const std::vector<char>& selected,
                             const std::vector<char>& btagged) const;

  ClassDefOverride(CustomFrame, 1);
};
