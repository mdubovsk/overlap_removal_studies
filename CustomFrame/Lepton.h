#pragma once

#include "Math/Vector4D.h"

class Lepton   {
    public:
        Lepton() = delete;
        Lepton(const ROOT::Math::PtEtaPhiEVector &four_momentum, int charge, int pdgId);

        const ROOT::Math::PtEtaPhiEVector &four_momentum() const;
        void set_four_momentum(const ROOT::Math::PtEtaPhiEVector &four_momentum);

        float Pt() const;
        float Eta() const;
        float Phi() const;
        float E() const;

        int charge() const;
        void set_charge(int charge);

        int pdgId() const;
        void set_pdgId(int pdgId);

        bool is_electron() const;
        bool is_muon() const;

    private:
        ROOT::Math::PtEtaPhiEVector m_four_momentum;
        int m_charge;
        int m_pdgId;
};