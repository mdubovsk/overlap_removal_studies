#pragma once

class PhysicsConstants  {
    public:
        static constexpr float top_mass = 172'500;
        static constexpr float w_mass = 80'385;
};
