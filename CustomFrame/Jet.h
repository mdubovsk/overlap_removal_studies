#pragma once

#include "Math/Vector4D.h"

class Jet   {
    public:
        Jet() = delete;
        Jet(const ROOT::Math::PtEtaPhiEVector &four_momentum, bool is_btagged);

        const ROOT::Math::PtEtaPhiEVector &four_momentum() const;
        void set_four_momentum(const ROOT::Math::PtEtaPhiEVector &four_momentum);

        float Pt() const;
        float Eta() const;
        float Phi() const;
        float E() const;

        bool is_btagged() const;
        void set_is_btagged(bool is_btagged);

    private:
        ROOT::Math::PtEtaPhiEVector m_four_momentum;
        bool m_is_btagged;

};